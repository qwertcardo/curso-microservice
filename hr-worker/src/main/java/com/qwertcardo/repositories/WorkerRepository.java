package com.qwertcardo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.qwertcardo.domain.Worker;

public interface WorkerRepository extends JpaRepository<Worker, Long> {

}
