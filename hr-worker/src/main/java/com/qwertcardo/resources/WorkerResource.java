package com.qwertcardo.resources;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qwertcardo.domain.Worker;
import com.qwertcardo.repositories.WorkerRepository;

@RestController
@RequestMapping(value = "/workers")
public class WorkerResource {
	private static Logger logger = LoggerFactory.getLogger(WorkerResource.class);
	
	@Autowired
	Environment env;

	@Autowired
	WorkerRepository repository;
	
	@GetMapping
	public ResponseEntity<List<Worker>> findAll() {
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(this.repository.findAll());
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Worker> findByid(@PathVariable Long id) {
		logger.info("PORT = " + this.env.getProperty("local.server.port"));
		
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(this.repository.findById(id).get());
	}
}
