package com.qwertcardo.hrpayroll.domain;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Worker implements Serializable {
	private static final long serialVersionUID = -5819644835156677864L;
	
	private Long id;
	private String name;
	private Double dailyIncome;
}
